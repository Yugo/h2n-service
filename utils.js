const Utils = {
    getDate: date => {
        const year = date.getFullYear();
        let month = date.getMonth() + 1;
        month = month < 10 ? '0' + month : month;
        let day = date.getDate();
        day = day < 10 ? '0' + day : day;
        return `${day}.${month}.${year}`;
    },
    generateToken: () => {
        let token;
        const tokenLength = 6;
        const tokenMultiplier = 10 ** tokenLength;
        const minimalTokenSize = tokenMultiplier / 10;
        token = Math.trunc(Math.random() * tokenMultiplier);
        if (token < minimalTokenSize) {
            token += minimalTokenSize;
        }
        return token;
    }
};

module.exports = Utils;