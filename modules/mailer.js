const mailer = require('nodemailer');
const config = require('../config');

class Mailer {
    constructor() {
        this.mailer = mailer;
        this.transporter = this.mailer.createTransport(config.mailing.sender);
    }

    getEmailOptions(subject, message, to) {
        let html = '';
        if (message && Array.isArray(message)) {
            message.forEach(msg => html += `<p>${msg}</p>`);
        } else {
            html += `<p>${message}</p>`;
        }

        return Object.assign(config.mailing.meta, {html, subject, to});
    }

    sendMail(subject, messages, to, callback) {
        const options = this.getEmailOptions(subject, messages, to);
        this.transporter.sendMail(options, (err, info) => {
            if (err) { callback(err, null); }
            if (info) { callback(null, info); }
        });
    }
}

const m = new Mailer();

module.exports = m;