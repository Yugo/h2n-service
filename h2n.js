const fs = require('fs');
const r = require('request').defaults({jar: true, forever: true});
const mailer = require('./modules/mailer');
const mongo = require('mongodb');
const MongoClient = mongo.MongoClient;
const dict = require('./constants');

const config = require('./config');

function parseToken(html) {
    const re = /name="__RequestVerificationToken" type="hidden" value="(.+)"/;
    return html.match(re)[1];
}

function parseMessage(html) {
    console.log(html);
    const re = /toastr\['(success|error)']\('(.+)', 'Pack/;
    let htmlArr = html.match(re);
    let string = htmlArr ? htmlArr[2] : 'Could not parse message';
    return string.replace(/&#x27;/g, "\'");
}

function getOptions(url, form, token) {
    const options = { url };
    if (form) {
        options.form = form;
        if (token) {
            options.form['__RequestVerificationToken'] = token;
        }
    }
    return options;
}

function isSucceeded(string) {
    const re = /successfully bound|removed/gi;
    return re.test(string);
}

function getDayEnd() {
    const now = new Date();
    now.setHours(23);
    now.setMinutes(59);
    now.setSeconds(59);
    return now.setMilliseconds(999);
}

function authorizeAndPost(action, accounts, cb) {
    r(config.url.login, (err, res, body) => {
        if (err && res.statusCode !== 200) {
            cb('Hand2Note: Can\'t reach service', null);
            return;
        }

        const token = parseToken(body);
        const options = getOptions(config.url.login, config.creds, token);

        r.post(options, (err, res, body) => {
            if (err && res.statusCode !== 302) {
                cb('Hand2Note: Authorization error', null);
                return;
            }
            handleAction(action, accounts, cb);
        });
    });
}

function handleAction(action, accounts, cb) {
    r(config.url.profile, (err, res, body) => {
        if (err) {
            cb('Hand2Note: Can\'t reach profile page', null);
            return;
        }

        const token = parseToken(body);
        let messages = [];

        accounts.forEach((acc, index) => {
            const options = getOptions(config.url[action], acc, token);

            r.post(Object.assign({}, options, { followAllRedirects: true }), (err, res, body) => {
                if (err) {
                    cb(err, null);
                    return;
                }
                const msg = parseMessage(body);

                // Проверка была ли проведена какая-то операция с аккаунтом
                if (!isSucceeded(msg) && accounts.length === 1) {
                    cb(`Hand2Note Error: ${msg}`, null);
                    return;
                }

                messages.push(msg);
                if (index === (accounts.length - 1)) {
                    const subject = accounts.length === 1 ? 'H2N Account Update' : 'H2N Update Summary';

                    mailer.sendMail(subject, messages, config.mailing.default.to,
                        (err, info) => {
                            if (err) cb(null, 'Error sending email');
                            if (info) cb(null, 'Email with results sent to pro.h2n@gmail.com');
                        }
                    );
                }
            });
        });
    })
}

async function runAgainsDB(cb) {
    let client;

    try {
        client = await MongoClient.connect(config.db.root);

        const db = client.db(config.db.name);
        const expirationDateTimestamp = getDayEnd();
        let foundAccounts = await db.collection('accounts').find().toArray();

        if (foundAccounts && foundAccounts.length) {
            foundAccounts = foundAccounts.filter(acc => {
                const active_to = acc.active_to.getTime();
                return active_to < expirationDateTimestamp && acc.status === 'active';
            });
        }
        if (foundAccounts.length) {
        	const accounts = foundAccounts.map(a => ({ Email: a.email, PackId: a.group + a.limit}));

        	for (let i = 0; i < foundAccounts.length; i++) {
            	let account = foundAccounts[i];

            	const r = await db.collection('accounts')
        			.updateOne({ _id: new mongo.ObjectID(account._id) }, { $set: {status: 'inactive'} });
            }
            authorizeAndPost(dict.h2nAction.UNBIND, accounts, cb);
        } else {
            cb(null, true);
        }
    } catch(e) {
        cb(e, null);
        return;
    }
    client.close();
}

module.exports.authorizeAndPost = authorizeAndPost;
module.exports.runAgainstDB = runAgainsDB;
