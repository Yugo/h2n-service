const express = require('express');
const router = express.Router();
const fs = require('fs');
const exec = require('child_process').exec;
const path = require('path');
const mongo = require('mongodb');
const MongoClient = mongo.MongoClient;
const upload = require('multer')();
const fileUpload = require('multer')({ dest: path.join(__dirname, '..', 'upload') });
const Utils = require('../utils');
const h2nService = require('../h2n').authorizeAndPost;
const sync = require('../h2n').runAgainstDB;
const dict = require('../constants');

const url = 'mongodb://localhost:27017';
const dbName = 'h2n';

router.use((req, res, next) => {
    if (req.headers['h2n-session-token'] != req.session.token) { res.sendStatus(403); }
    else { next(); }
});

router.use('/sync', (req, res, next) => {
    sync((err, info) => {
        if (err) { res.status(500).send(err); }
        if (info) { res.status(200).send(info); }
    });
});

// EXPORT IMPORT
router.use('/export', (req, res, next) => {
    const hash = Date.now().toString(16);
    const fileName = `accounts.${hash}.json`;
    const fields = ['email', 'group', 'limit', 'status', 'active_from', 'active_to'];

    exec(`mongoexport --db h2n --collection accounts -f ${fields.join(',')} --out export/${fileName}`,
        (err, stdout, stderr) => {
            if (err) {
                console.log(err);
                res.sendStatus(500);
            } else {
                res.sendFile(path.join(__dirname, '..', 'export', fileName), err => {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log('Sent: ' + fileName);
                    }
                });
            }
    });
});

router.post('/import', fileUpload.single('accounts'), (req, res, next) => {
    if (req.file) {
        const oldPath = path.join(req.file.destination, req.file.filename);
        const newPath = path.join(req.file.destination, req.file.originalname);

        fs.renameSync(oldPath, newPath, err => {
            if (err) throw err;
        });
        exec(`mongoimport --drop --db h2n --collection accounts --file upload/${req.file.originalname}`, (err, stdout, stderr) => {
            if (err) {
                console.error(`exec error: ${err}`);
                return;
            }
            console.log(`stdout: ${stdout}`);
            console.log(`stderr: ${stderr}`);
        });
        res.sendStatus(200);
    } else {
        res.sendStatus(400);
    }
});

function deleteHandler(collection) {
    return function(req, res, next) {
        const _id = req.params.id;
        let item;

        (async function() {
            let client;

            try {
                client = await MongoClient.connect(url);

                const db = client.db(dbName);
                let r;

                if (collection === 'accounts') {
                    item = await db.collection(collection).findOne({_id: new mongo.ObjectID(_id)});
                    r = await db.collection(collection).deleteOne({_id: new mongo.ObjectID(_id)});
                } else {
                    r = await db.collection(collection).deleteOne({ _id });
                }

                if (r.deletedCount === 1) {
                    res.status(200).send('Successfully deleted');
                } else {
                    res.sendStatus(404);
                }

            } catch(err) {
                res.status(500).send('DB error');
            }

            client.close();
        })();
    }
}

function addHandler(collection) {
    return function(req, res, next) {
        let item;
        if (collection === 'accounts') {
            if (!req.body.email || !req.body.group || !req.body.limit || !req.body.end_date) {
                res.status(400).send('Missing required field');
                return;
            }
            item = {
                email: req.body.email,
                limit: req.body.limit,
                group: req.body.group,
                status: req.body.status,
                active_from: req.body.start_date ? new Date(req.body.start_date) : new Date(),
                active_to: new Date(req.body.end_date)
            };

        } else if (collection === 'users') {
            if (!req.body.id || !req.body.permissions) {
                res.status(400).send('Missing required field');
                return;
            }
            item = {
                _id: req.body.id,
                permissions: req.body.permissions
            };
        } else {
            if (!req.body.id || !req.body.name) {
                res.status(400).send('Missing required field');
                return;
            }
            item = {
                _id: req.body.id,
                name: req.body.name
            };
        }

        (async function() {
            let client;

            try {
                client = await MongoClient.connect(url);

                const db = client.db(dbName);
                let r;

                if (collection === 'accounts') {
                    console.log(item);

                    r = await db.collection('accounts').updateOne(
                        { email: item.email, group: item.group, limit: item.limit },
                        item,
                        { upsert: true });

                    console.log(r);
                    if (r.upsertedCount === 1) {
                        if (!req.body.start_date && req.body.status && req.body.status !== 'active') {
                            h2nService(dict.h2nAction.BIND, [{
                                Email: item.email,
                                PackId: `${item.group}${item.limit}`
                            }], (err, info) => {
                                if (err) {
                                    console.log(err);
                                }
                                if (info) {
                                    console.log(info);
                                }
                            });
                        }
                        res.sendStatus(200);
                    } else if (r.modifiedCount === 1) {
                        res.sendStatus(200);
                    } else {
                        res.status(400).send('Bad Request');
                    }


                } else {
                    r = await db.collection(collection).insertOne(item);
                    if (r.insertedCount === 1) {
                        res.sendStatus(200);
                    } else {
                        res.status(400).send('Bad Request');
                    }
                }

            } catch(err) {
                res.status(500).send(err);
            }

            client.close();
        })();
    }
}

function updateHandler(collection) {
    return function(req, res, next) {
        const _id = req.params.id;

        let item;
        if (collection === 'accounts') {
            item = {
                group: req.body.group,
                limit: req.body.limit,
                status: req.body.status ? req.body.status : 'inactive',
                active_from: req.body.start_date ? new Date(req.body.start_date) : new Date(),
                active_to: new Date(req.body.end_date),
            }
        } else if (collection === 'users') {
            if (!req.body.permissions) {
                res.status(400).send('Missing required field');
                return;
            }
            item = { permissions: req.body.permissions };
        } else {
            if (!req.body.name) {
                res.status(400).send('Missing required field');
                return;
            }
            item = { name: req.body.name };
        }
        (async function() {
            let client;

            try {
                client = await MongoClient.connect(url);

                const db = client.db(dbName);
                let r;

                if (collection === 'accounts') {
                    r = await db.collection(collection).updateOne({_id: new mongo.ObjectID(_id)}, { $set: item });
                } else {
                    r = await db.collection(collection).updateOne({_id}, { $set: item });
                }

                if (r.modifiedCount === 1) {
                    res.status(200).send('Successfully updated');
                } else {
                    res.status(400).send('Bad Request');
                }

            } catch(err) {
                res.status(500).send('DB error');
            }

            client.close();
        })();
    }
}

function activationHandler(status) {
    return function(req, res, next) {
        const _id = req.params.id;

        (async function() {
            let client;

            try {
                client = await MongoClient.connect(url);

                const db = client.db(dbName);
                let item = await db.collection('accounts').findOne({_id: new mongo.ObjectID(_id)});

                let r = await db.collection('accounts').updateOne({_id: new mongo.ObjectID(_id)}, { $set: {status} });
                if (r.modifiedCount === 1) {
                    h2nService(status === 'inactive' ? dict.h2nAction.UNBIND : dict.h2nAction.BIND,
                        [{
                            Email: item.email,
                            PackId: `${item.group}${item.limit}`
                        }], (err, info) => {
                            if (err) { res.status(500).send(err); }
                            else { res.sendStatus(200); }
                        }
                    );
                } else {
                    res.status(400).send('Bad Request');
                }
            } catch(err) {
                res.status(500).send('DB error');
            }

            client.close();
        })();
    }
}



// GROUPS
router.post('/groups', upload.array(), addHandler('groups'));
router.delete('/groups/:id', deleteHandler('groups'));
router.patch('/groups/:id', upload.array(), updateHandler('groups'));

// LIMITS
router.post('/limits', upload.array(), addHandler('limits'));
router.delete('/limits/:id', deleteHandler('limits'));
router.patch('/limits/:id', upload.array(), updateHandler('limits'));

// ACCOUNTS
router.post('/accounts', upload.array(), addHandler('accounts'));
router.delete('/accounts/:id', deleteHandler('accounts'));
router.patch('/accounts/:id', upload.array(), updateHandler('accounts'));

// USERS
router.post('/users', upload.array(), addHandler('users'));
router.delete('/users/:id', deleteHandler('users'));
router.patch('/users/:id', upload.array(), updateHandler('users'));

// ACTIVATION
router.get('/deactivate/:id', activationHandler('inactive'));
router.get('/activate/:id', activationHandler('active'));

module.exports = router;