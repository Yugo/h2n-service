class Modal {
    constructor(alert) {
        this.root = document.querySelector('.modal');
        this.shadow = document.querySelector('.modal-shadow');
        this.form = document.querySelector('.modal-form');
        this.alert = alert;

        this.init();
    }

    open(activeForm, url) {
        this.root.dataset.view = 'on';
        [...this.root.querySelectorAll('.btn')].forEach(btn => btn.style.display = 'none');
        this.root.querySelector(`.${activeForm}`).style.display = '';
        this.form.querySelector(`.${activeForm}-form`).classList.add('-active');
        this.form.action = url;
    }

    close() {
        this.root.dataset.view = 'off';
        this.hideError();
        setTimeout(() => {
            this.form.querySelector(`.-active`).classList.remove('-active');
        }, 360);
    }

    setFormValues(data) {
        Object.keys(data).forEach(key => {
            console.log(data[key]);
            console.log(this.form[key].value);
            this.form[key].value = data[key];
        });
    }

    clearFormValues() {
        [...this.form.querySelectorAll('[name]')].forEach(field => {
            if (field.name === 'status') {
                field.querySelector('[value=inactive]').setAttribute('selected', 'true');
            } else {
                field.value = ''
            }
        });
    }

    setFormMethod(method) {
        this.form.method = method;
    }

    setReadonly(bool, ...fields) {
        fields.forEach(field => {
            if (this.form[field]) {
                if (bool) {
                    this.form[field].setAttribute('readonly', 'true');
                } else {
                    this.form[field].removeAttribute('readonly');
                }
            }
        });
    }

    showError(err) {
        this.form.querySelector('.error-message').textContent = err;
    }

    hideError() {
        this.form.querySelector('.error-message').textContent = '';
    }

    collect(fields) {
        const data = new FormData();
        fields.forEach(field => data.set(field, this.form.querySelector(`[name=${field}]`).value));
        return data;
    }

    attachListeners() {
        const self = this;
        this.shadow.addEventListener('click', this.close.bind(this));
        this.form.addEventListener('click', e => {
            e.preventDefault();
            if (e.target.tagName !== 'BUTTON') {
                return;
            }
            const submitType = e.target.className.replace(/\sbtn/, '');
            const form = this.form.querySelector(`.${submitType}-form`);
            const fields = [...form.querySelectorAll('[name]')].map(el => el.name);

            const data = this.collect(fields);
            const method = this.form.method === 'post' ? 'POST' : 'PATCH';

            const xhr = new XMLHttpRequest();
            xhr.open(method, this.form.action);
            xhr.setRequestHeader('H2N-Session-Token', localStorage.getItem('token'));
            xhr.onreadystatechange = function() {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    if (xhr.status === 200) {
                        self.close();
                        setTimeout(() => window.location.reload(), 400);
                    } else {
                        self.showError(xhr.response);
                    }
                }
            };
            xhr.send(data);
        });
    }

    removeListeners() {
        this.shadow.removeEventListener('click', this.close.bind(this));
    }

    init() {
        this.attachListeners();
    }
}

class Table {
    constructor(type, modal, alert) {
        this.type = type;
        this.table = document.querySelector(`.${type}-table`);
        this.modal = modal;
        this.alert = alert;

        this.addRowBtn = this.table.parentNode.querySelector('.add');
        this.formSize = type === 'accounts' ? 'large' : 'short';

        this._init();
    }

    getRowData(target) {
        const row = target.closest('tr');
        return {
            html: row,
            id: row.id || row.querySelector('td').textContent
        };
    }

    getMethod(target) {
        const re = /-btn/;
        return target.className.match(/(.*)-btn/)[1];
    }

    prepareDate(dateString) {
        return dateString.split('.').reverse().join('-');
    }

    prepareStatus(raw) {
        return raw === 'Активен' ? 'active' : 'inactive';
    }

    getActivationButtonState(row) {
        return row.querySelector('.activation-btn').dataset.action;
    }

    handleActivation(target) {
        const row = this.getRowData(target);
        const activation = this.getActivationButtonState(row.html);
        this.alert.loading('Загрузка...');

        const xhr = new XMLHttpRequest();
        xhr.open('GET', `/api/${activation}/${row.id}`);
        xhr.setRequestHeader('H2N-Session-Token', localStorage.getItem('token'));
        xhr.onreadystatechange = () => {
            this.alert.clear();
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    window.location.reload();
                } else {
                    this.alert.error(xhr.response).timeout(3000);
                }
            }
        };
        xhr.send();
    }

    deleteRow(target) {
        const row = this.getRowData(target);
        this.alert.loading('Удаление...');

        const xhr = new XMLHttpRequest();
        xhr.open('DELETE', `/api/${this.type}/${row.id}`);
        xhr.setRequestHeader('H2N-Session-Token', localStorage.getItem('token'));
        xhr.onreadystatechange = () => {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                this.alert.clear();
                if (xhr.status === 200) {
                    row.html.remove();
                    this.alert.success(xhr.response).timeout(3000);
                }
            } else {
                this.alert.error(xhr.response).timeout(3000);
            }
        };
        xhr.send();
    }

    collect(row) {
        const children = row.querySelectorAll('td');

        if (this.type === 'accounts') {
            return {
                email: children[0].textContent,
                group: children[1].textContent,
                limit: children[2].textContent,
                status: this.prepareStatus(children[3].textContent),
                start_date: this.prepareDate(children[4].textContent),
                end_date: this.prepareDate(children[5].textContent)
            }
        } else if (this.type === 'users') {
            return {
                id: children[0].textContent,
                permissions: children[1].textContent
            }
        } else {
            return {
                id: children[0].textContent,
                name: children[1].textContent
            }
        }
    }

    takeAction(target, method) {
        if (method === 'delete') {
            this.deleteRow(target);
        } else if (method === 'put') {
            const row = this.getRowData(target);

            this.modal.open(this.formSize, `/api/${this.type}/${row.id}`);
            this.modal.setFormValues(this.collect(row.html));
            this.modal.setFormMethod('PUT');
            this.modal.setReadonly(true, 'id', 'email');
        } else {
            this.handleActivation(target);
        }
    }

    _registerEvents() {
        this.table.addEventListener('click', e => {
            const btn = e.target.closest('button');
            if (!btn || btn.tagName !== 'BUTTON') {
                return;
            }

            const method = this.getMethod(btn);
            this.takeAction(btn, method);
        });

        this.addRowBtn.addEventListener('click', () => {
            this.modal.open(this.formSize, `/api/${this.type}`);
            this.modal.setFormMethod('POST');
            this.modal.clearFormValues();
            this.modal.setReadonly(false, 'id', 'email', '');
        });
    }

    _init() {
        this._registerEvents();
    }
}

class Auth {
    constructor(alert) {
        this.form = document.forms[0];
        this.alert = alert;

        this._init();
    }

    getData() {
        const data = new FormData();
        const input = this.getVisibleInput();

        data.set(input.name, input.value);
        return data;
    }

    send(url, data, cb) {
        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = cb(xhr);
        xhr.open('POST', url);
        xhr.send(data);
    }

    getVisibleInput() {
        const visibleRow = this.form.querySelector('.form-row:not(.-hidden)');
        return visibleRow.querySelector('input');
    }

    saveToken(token) {
        localStorage.setItem('token', token);
        return token;
    }

    switchFields() {
        const visibleRow = this.form.querySelector('.form-row:not(.-hidden)');
        const hiddenRow = this.form.querySelector('.form-row.-hidden');

        visibleRow.classList.add('-hidden');
        hiddenRow.classList.remove('-hidden');
    }

    _registerEvents() {
        this.form.addEventListener('submit', e => {
            e.preventDefault();
            const input = this.getVisibleInput();
            this.alert.loading('Загрузка...');

            if (input.name === 'email') {
                this.send('/auth', this.getData(), xhr => () => {
                    if (xhr.readyState === XMLHttpRequest.DONE) {
                        if (xhr.status === 200) {
                            this.alert.clear();
                            this.switchFields();
                            this.form.querySelector('button').textContent = 'Войти';
                        } else {
                            this.alert.error(xhr.response).timeout(3000);
                        }
                    }
                });
            } else {
                this.send('/auth/verify', this.getData(), xhr => () => {
                    if (xhr.readyState === XMLHttpRequest.DONE) {
                        if (xhr.status === 200) {
                            this.saveToken(input.value);
                            window.location.href = '/';
                        } else {
                            this.alert.error(xhr.response).timeout(3000);
                        }
                    }
                });
            }
        });
    }

    _init() {
        this._registerEvents();
    }
}

class Alert {
    constructor() {
        this.root = document.querySelector('.alert');
        this.text = null;
        this.timer = null;
    }

    // loading, success, error
    _appendMessage(type, message) {
        this.clear();
        this.text = document.createTextNode(message);
        this.root.appendChild(this.text);
        this.root.classList.add(`-${type}`);
        this.root.classList.remove('-hidden');
    }

    loading(message) {
        this._appendMessage('loading', message);
        return this;
    }

    success(message) {
        this._appendMessage('success', message);
        return this;
    }

    error(message) {
        this._appendMessage('error', message);
        return this;
    }

    clear() {
        if (this.text) {
            this.text.remove();
        }
        this.root.className = 'alert -hidden';
    }

    timeout(ms) {
        if (this.timer) {
            clearTimeout(this.timer);
            this.timer = null;
        }

        this.timer = setTimeout(this.clear.bind(this), ms);
    }
}

class Actions {
    constructor(cl, alert) {
        this.container = document.querySelector(`.${cl}`);
        this.alert = alert;

        this._init();
    }

    getAction(target) {
        return target.className.split(' ').filter(cl => cl !== 'icon-btn')[0];
    }

    getMessage(action) {
        return action === 'export' ? 'Файл готов для загрузки' : 'Синхронизация с БД прошла успешно';
    }

    request(action, message, callback) {
        alert.loading('Загрузка...');

        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = () => {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    this.alert
                        .success(message)
                        .timeout(3000);
                    callback(xhr);
                } else {
                    this.alert.error(xhr.response).timeout(5000);
                }
            }
        };
        xhr.open('GET', `/api/${action}`);
        xhr.setRequestHeader('H2N-Session-Token', localStorage.getItem('token'));
        xhr.send();
    }

    openUploadWindow() {
        this.container.querySelector('[name=accounts]').click();
    }

    upload() {
        const form = this.container.querySelector('.accounts-form');
        if (form.accounts.value) {
            const data = new FormData(form);

            const xhr = new XMLHttpRequest();
            xhr.onreadystatechange = () => {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    if (xhr.status === 200) {
                        setTimeout(() => {
                            window.location.href = '/';
                        }, 1000);
                    } else {
                        this.alert.error(xhr.response).timeout(5000);
                    }
                }
            };
            xhr.open('POST', `/api/import`);
            xhr.setRequestHeader('H2N-Session-Token', localStorage.getItem('token'));
            xhr.send(data);
        }
    }

    download(xhr) {
        const blob = new Blob([xhr.response], {type: 'application/json'});
        let a = document.createElement("a");
        a.style = "display: none";
        document.body.appendChild(a);

        let url = window.URL.createObjectURL(blob);
        a.href = url;
        a.download = 'accounts.json';

        a.click();

        window.URL.revokeObjectURL(url);
        a.remove();
    }

    _registerEvents() {
        this.container.addEventListener('click', e => {
            const target = e.target.closest('button');
            if (target && target.tagName === 'BUTTON') {
                e.preventDefault();
                const action = this.getAction(target);
                if (action === 'import') {
                    this.openUploadWindow();
                } else {
                    const message = this.getMessage(action);
                    this.request(action, message, action === 'export' ? this.download.bind(this) : () => {});
                }
            }
        });
        this.container.querySelector('[name=accounts]').addEventListener('change', this.upload.bind(this));
    }

    _init() {
        this._registerEvents();
    }
}

const alert = new Alert();


if (document.querySelector('.modal')) {
    let modal = new Modal('modal', alert);
    ['accounts', 'groups', 'limits', 'users'].forEach(table => {
        if (document.querySelector(`.${table}`)) {
            new Table(table, modal, alert)
        }
    });

    const actions = new Actions('actions', alert);
}

if (document.querySelector('.auth-form')) {
    new Auth(alert);
}