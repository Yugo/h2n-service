const h2nDictionary = {
    status: {
        inactive: 'Не активен',
        active: 'Активен'
    },
    h2nAction: {
        BIND: 'addLicense',
        UNBIND: 'removeLicense'
    }
};

module.exports = h2nDictionary;