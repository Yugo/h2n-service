const express = require('express');
const router = express.Router();
const MongoClient = require('mongodb').MongoClient;
const Utils = require('../utils');
const dict = require('../constants');

const url = 'mongodb://localhost:27017',
    dbName = 'h2n';

const data = {};

function getTimeBorders(year, month) {
    let period;
    if (!year || !month) {
        period = new Date();
    } else {
        period = new Date(year, month, 1);
    }

    const start = new Date(period.getFullYear(), period.getMonth(), 1);
    const end = new Date(period.getFullYear(), period.getMonth() + 1, 0);
    return { start, end };
}

router.get('/', function(req, res, next) {
    if (req.session.permissions !== 'admin' || !req.session.token) {
        res.redirect('/auth');
    } else {
        (async function() {
            let client;

            try {
                client = await MongoClient.connect(url);

                const db = client.db(dbName);

                const borders = getTimeBorders(req.query.year, req.query.month);

                data.accounts = await db.collection('accounts').find({
                    active_from: { $gte: borders.start, $lte: borders.end }
                }).toArray();
                data.accounts = data.accounts.map(acc => {
                    acc.active_to = Utils.getDate(acc.active_to);
                    acc.active_from = Utils.getDate(acc.active_from);
                    acc.status = dict.status[acc.status];
                    return acc;
                });
                data.groups = await db.collection('groups').find().toArray();
                data.limits = await db.collection('limits').find().toArray();
                data.status = await db.collection('status').find().toArray();
                data.periods = await db.collection('accounts').aggregate([
                    { $project: {
                        month: {$month: "$active_from"},
                        year: {$year: "$active_from"}
                    }},
                    { $group: {
                        _id: { month: "$month", year: "$year" }
                    } },
                    { $sort: { '_id.year': -1, '_id.month': -1 } }
                ]).toArray();

                res.render('index', { data, title: 'Dashboard' });
            } catch(err) {
                console.log(err);
                res.status(500).send('DB error');
            }

            client.close();
        })();
    }
});

module.exports = router;