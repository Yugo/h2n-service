const express = require('express');
const router = express.Router();
const MongoClient = require('mongodb').MongoClient;
const Utils = require('../utils');
const dict = require('../constants');

const url = 'mongodb://localhost:27017',
    dbName = 'h2n';

router.get('/', function(req, res, next) {
    if (req.session.permissions !== 'admin' || !req.session.token) {
        res.redirect('/auth');
    } else {
        (async function() {
            let client;

            try {
                client = await MongoClient.connect(url);

                const db = client.db(dbName);
                const users = await db.collection('users').find().toArray();

                res.render('users', { users, title: 'Users' });
            } catch(err) {
                console.log(err);
                res.status(500).send('DB error');
            }

            client.close();
        })();
    }
});

module.exports = router;