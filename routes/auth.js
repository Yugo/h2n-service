const express = require('express');
const router = express.Router();
const MongoClient = require('mongodb').MongoClient;
const mailer = require('../modules/mailer');
const Utils = require('../utils');

const upload = require('multer')();

const url = 'mongodb://localhost:27017',
    dbName = 'h2n';

router.get('/', function(req, res, next) {
    res.render('auth', { title: "Авторизация" });
});

router.post('/', upload.array(), function(req, res, next) {
    const user = req.body;
    if (!user.email) {
        res.status(400).send('Missing required field');
        return;
    }

    (async function() {
        let client;

        try {
            client = await MongoClient.connect(url);

            const db = client.db(dbName);
            let foundUser = await db.collection('users').find({_id: user.email}).toArray();
            foundUser = foundUser && foundUser.length ? foundUser[0] : null;

            if (foundUser) {
                req.session.permissions = foundUser.permissions;
                req.session.check_token = Utils.generateToken();
                mailer.sendMail(
                    'H2N Authorization Token',
                    req.session.check_token,
                    user.email,
                    (err, info) => {
                        if (err) { res.status(500).send('Error sending email'); }
                        if (info) { res.sendStatus(200); }
                    }
                );
            } else {
                res.status(403).send('No user in DB');
            }
        } catch(err) {
            console.log(err);
            res.status(500).send('DB error')
        }

        client.close();
    })();
});

router.post('/verify', upload.array(), function(req, res, next) {
    const token = req.body.token;
    if (token != req.session.check_token) {
        res.status(403).send('Invalid token');
    } else {
        req.session.token = token;
        req.session.check_token = '';
        res.sendStatus(200);
    }
});

module.exports = router;